package nb.scode.tanyasoalmvp.di.modules;

import android.app.Application;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Aksiom on 6/29/2016.
 */
@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application providesApplication() {
        return mApplication;
    }

}
