package nb.scode.tanyasoalmvp.di.components;

import android.content.SharedPreferences;

import dagger.Component;
import nb.scode.tanyasoalmvp.di.modules.AppModule;
import nb.scode.tanyasoalmvp.di.modules.DataModule;
import nb.scode.tanyasoalmvp.di.modules.NetworkModule;
import nb.scode.tanyasoalmvp.di.scope.PerApp;
import nb.scode.tanyasoalmvp.network.ApiService;
import nb.scode.tanyasoalmvp.util.NetworkManager;

/**
 * Created by Aksiom on 6/29/2016.
 */
@PerApp
@Component(modules = {AppModule.class, NetworkModule.class, DataModule.class})
public interface AppComponent {

    ApiService apiService();

    NetworkManager networkManager();

    SharedPreferences sharepPref();

}
