package nb.scode.tanyasoalmvp.di.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.PerApp;

/**
 * Created by Aksiom on 6/29/2016.
 */
@Module
public class DataModule {

    public DataModule() {

    }

    @Provides
    @PerApp
    SharedPreferences providesSharedPref(Application application){
        final String PREF_NAME = "worldhere";
        return application.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }



}
