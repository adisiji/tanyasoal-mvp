package nb.scode.tanyasoalmvp.di.modules;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dagger.Module;
import dagger.Provides;
import java.util.concurrent.TimeUnit;
import nb.scode.tanyasoalmvp.di.scope.PerApp;
import nb.scode.tanyasoalmvp.network.ApiService;
import nb.scode.tanyasoalmvp.util.HttpInterceptor;
import nb.scode.tanyasoalmvp.util.NetworkManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Aksiom on 6/29/2016.
 */
@Module public class NetworkModule {

  private Context context;

  public NetworkModule(Context context) {
    this.context = context;
  }

  @Provides @PerApp Gson provideGson() {
    return new GsonBuilder().setLenient().create();
  }

  @Provides @PerApp NetworkManager networkManager() {
    return new NetworkManager(context);
  }

  @Provides @PerApp OkHttpClient okHttpClient() {
    HttpInterceptor interceptor = new HttpInterceptor();
    return new OkHttpClient.Builder().addInterceptor(interceptor)
        .readTimeout(60L, TimeUnit.SECONDS)
        .connectTimeout(60L, TimeUnit.SECONDS)
        .build();
  }

  @Provides @PerApp ApiService provideApiService(OkHttpClient okHttpClient) {
    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiService.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .build();
    return retrofit.create(ApiService.class);
  }
}
