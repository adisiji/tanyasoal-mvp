package nb.scode.tanyasoalmvp;

import android.app.Application;
import android.os.StrictMode;

import nb.scode.tanyasoalmvp.data.DaggerDataComponent;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataModule;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataModule;
import nb.scode.tanyasoalmvp.di.components.AppComponent;
import nb.scode.tanyasoalmvp.di.components.DaggerAppComponent;
import nb.scode.tanyasoalmvp.di.modules.AppModule;
import nb.scode.tanyasoalmvp.di.modules.NetworkModule;
import timber.log.Timber;

/**
 * Created by neobyte on 2/19/2017.
 */

public class App extends Application {

    private static DataComponent dataComponent;

    @Override
    public void onCreate() {
      super.onCreate();
      initInjector();
      if (BuildConfig.DEBUG) {
        Timber.plant(new Timber.DebugTree());
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
            .detectAll()
            .penaltyLog()
            .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
            .detectAll()
            .penaltyLog()
            .build());
      }
    }

    private void initInjector(){
      AppComponent appComponent = DaggerAppComponent.builder()
          .appModule(new AppModule(this))
          .networkModule(new NetworkModule(this))
          .build();
      dataComponent = DaggerDataComponent.builder()
          .appComponent(appComponent)
          .cloudDataModule(new CloudDataModule())
          .prefDataModule(new PrefDataModule())
          .build();

    }

    public static DataComponent getDataComponent(){
        return dataComponent;
    }
}
