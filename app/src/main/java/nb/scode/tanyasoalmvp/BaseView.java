package nb.scode.tanyasoalmvp;

public interface BaseView<T> {

    void showError(String msg);

    void showLoading();

    void hideLoading();

}
