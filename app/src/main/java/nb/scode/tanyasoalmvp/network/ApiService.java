package nb.scode.tanyasoalmvp.network;

import io.reactivex.Observable;
import java.util.List;
import nb.scode.tanyasoalmvp.modelretro.BalanceUser;
import nb.scode.tanyasoalmvp.modelretro.BaseApiResponse;
import nb.scode.tanyasoalmvp.modelretro.ConversationRetro;
import nb.scode.tanyasoalmvp.modelretro.LoginResult;
import nb.scode.tanyasoalmvp.modelretro.PackageRetro;
import nb.scode.tanyasoalmvp.modelretro.PreQuestionRetro;
import nb.scode.tanyasoalmvp.modelretro.ProfileResponse;
import nb.scode.tanyasoalmvp.modelretro.QuestionRetro;
import nb.scode.tanyasoalmvp.models.Academics;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.json.JSONArray;
import org.json.JSONObject;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by User on 1/17/2017.
 */

public interface ApiService {

  String BASE_URL = "http://api.cariproject.com/api/v1/";
  //testertanyasoal1@gmail.com
  //testertanyasoal1

  @GET("academic") Observable<Academics> getPredefines();

  @POST("user/register") //pendaftaran user baru
  Observable<BaseApiResponse> signUp(@Body RequestBody body);

  @Headers("Content-Type:application/json")
  @POST("token/generate") Observable<LoginResult> login(@Body RequestBody body);

  @POST("user/forgot-password-validation") Observable<BaseApiResponse> forgotpwd(
      @Body RequestBody body);

  @POST("user/reset-password") Observable<BaseApiResponse> resetpwd(@Body RequestBody body);

  @GET("user/profile/{token}") Observable<ProfileResponse> getProfile(@Path("token") String token);

  @POST("user/profile/{token}") Observable<ProfileResponse> setProfile(@Path("token") String token,
      @Body RequestBody body);

  @FormUrlEncoded @POST("user/") Observable<BaseApiResponse> setProfile(
      @Query("token") String token, @Field("email") String email,
      @Field("first_name") String first_name, @Field("phone_number") String phone,
      @Field("dob") String birthday, @Field("academic_type_id") int academic_type,
      @Field("academic_level_id") int academic_level, @Field("school_name") String sekolah);

  @FormUrlEncoded @POST("voucher/request/") Observable<BaseApiResponse> reqVoucher(
      @Field("email") String email, @Field("phone_number") String phone,
      @Field("amount") long amount);

  @POST("voucher/use") Observable<BaseApiResponse> useVoucher(
      @Field("voucherCode") String voucherCode);

  @GET("services") Observable<JSONArray> getServices();

  @GET("packages") Observable<JSONObject> getPackageTanya();

  @POST("subscription/subscribe") Observable<BaseApiResponse> subscribe(
      @Query("token") String token);

  @GET("user/balance/{token}") Observable<BalanceUser> getBalance(@Path("token") String token);

  /////////////////////////////////////////////////////////
  //Question                                            //
  ///////////////////////////////////////////////////////
  @GET("question/predefines") Observable<PreQuestionRetro> getPreQuestion(
      @Query("token") String token);

  @Multipart @FormUrlEncoded @POST("question/ask-tutor") Observable<BaseApiResponse> askTutor(
      @Field("package_id") int package_id,
      @Field("lesson_id") int lesson_id, @Field("description") String description,
      @Field("number_of_question") int number_question,
      @Field("user_id") int userId,
      @Field("level_id") int levelId,
      @Field("subject_id") int subjectId,
      @Part("file_upload") MultipartBody.Part file);

  //Questions asked by me
  @GET("user/questions/{path}") Observable<List<QuestionRetro>> getMyQuestion(
      @Path("token") String token);

  //Tutor get open questions
  @GET("questions/open") Observable<List<QuestionRetro>> getTutorQuest(
      @Query("token") String token);

  //Tutor answer question
  @Multipart @FormUrlEncoded @POST("question/tutorAnswer") Observable<BaseApiResponse> tutorAnswer(
      @Query("token") String token, @Field("question_id") int quest_id,
      @Field("message") String message, @Field("claimed_answer_count") int answer_count,
      @Part("files1") MultipartBody.Part file1, @Part("files2") MultipartBody.Part file2);

  //Student response tutor answer
  @POST("question/studentAnswer") Observable<BaseApiResponse> studentAnswer(
      @Query("token") String token, @Field("question_id") int quest_id,
      @Field("message") String message);

  // Tutor respond to student answer, digunakan ketika tutor
  // hanya akan posting komentar saja, tidak menjawab dengan attachment
  Observable<BaseApiResponse> tutorAnswer(@Query("token") String token,
      @Field("question_id") int quest_id, @Field("message") String message);

  //Get conversations student & tutor
  @GET("question/conversations") Observable<List<QuestionRetro>> getConversation(
      @Query("token") String token);

  //Get conversation detail
  @GET("question/conversations/{conversation_id}") Observable<ConversationRetro> getConversationDetail(
      @Query("token") String token, @Path("conversation_id") int id);
}
