package nb.scode.tanyasoalmvp.data.clouddata;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.Cloud;
import nb.scode.tanyasoalmvp.network.ApiService;
import nb.scode.tanyasoalmvp.util.NetworkManager;

/**
 * Created by neobyte on 2/19/2017.
 */

@Module
public class CloudDataModule {

    @Provides
    @Cloud
    CloudDataRepo providesDataRepo(ApiService apiService, NetworkManager networkManager){
        return new CloudDataRepo(apiService,networkManager);
    }

}
