package nb.scode.tanyasoalmvp.data.prefdata;

import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.Prefs;

/**
 * Created by neobyte on 3/1/2017.
 */

@Module
public class PrefDataModule {

    @Provides
    @Prefs
    PrefDataRepo providesDataRepo(SharedPreferences sharedPreferences){
        return new PrefDataRepo(sharedPreferences);
    }

}
