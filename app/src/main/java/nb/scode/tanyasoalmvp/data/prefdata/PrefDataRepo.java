package nb.scode.tanyasoalmvp.data.prefdata;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Inject;
import timber.log.Timber;

/**
 * Created by neobyte on 3/1/2017.
 */

public class PrefDataRepo implements PrefDataTask{

    private static final String FCM_TOKEN = "FCM_TOKEN";
    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    private static final String USER_NAME = "USER_NAME";
    private final SharedPreferences sharedPreferences;

    @Inject
    public PrefDataRepo(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void saveFcmToken(@NonNull String token) {
        sharedPreferences.edit().putString(FCM_TOKEN,token).apply();
        Log.d("FCM TOKEN SAVED ==>", token);
    }

    @Override
    public String getFcmToken() {
        return sharedPreferences.getString(FCM_TOKEN,null);
    }

    @Override
    public void saveAcccessToken(@NonNull String token) {
        sharedPreferences.edit().putString(ACCESS_TOKEN, token).apply();
        Log.d("ACCESS TOKEN SAVED ==>", token);
    }

    @Override
    public String getAccessToken() {
        return sharedPreferences.getString(ACCESS_TOKEN,null);
    }

    @Override public void saveUsername(@NonNull String username) {
        sharedPreferences.edit().putString(USER_NAME, username).apply();
        Timber.i("Username SAVED ==> %s",username);
    }

    @Override public String getUsername() {
        return sharedPreferences.getString(USER_NAME, null);
    }
}
