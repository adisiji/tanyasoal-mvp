package nb.scode.tanyasoalmvp.data.prefdata;

import android.support.annotation.NonNull;

/**
 * Created by neobyte on 3/1/2017.
 */

public interface PrefDataTask {

    void saveFcmToken(@NonNull String token);

    String getFcmToken();

    void saveAcccessToken(@NonNull String token);

    String getAccessToken();

    void saveUsername(@NonNull String username);

    String getUsername();

}
