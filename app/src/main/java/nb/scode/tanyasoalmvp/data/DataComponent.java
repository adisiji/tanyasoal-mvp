package nb.scode.tanyasoalmvp.data;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataModule;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataModule;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataRepo;
import nb.scode.tanyasoalmvp.di.components.AppComponent;
import nb.scode.tanyasoalmvp.di.scope.Cloud;
import nb.scode.tanyasoalmvp.di.scope.Prefs;

/**
 * Created by neobyte on 2/19/2017.
 */

@Cloud
@Prefs
@Component(dependencies = AppComponent.class, modules = {CloudDataModule.class, PrefDataModule.class})
public interface DataComponent {

    CloudDataRepo getCloudRepo();

    PrefDataRepo getPrefRepo();

}
