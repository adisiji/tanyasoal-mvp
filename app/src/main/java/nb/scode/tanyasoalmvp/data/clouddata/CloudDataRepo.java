package nb.scode.tanyasoalmvp.data.clouddata;

import android.util.Log;
import com.google.common.graph.ElementOrder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.ResourceObserver;
import io.reactivex.schedulers.Schedulers;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import nb.scode.tanyasoalmvp.modelretro.BalanceUser;
import nb.scode.tanyasoalmvp.modelretro.BaseApiResponse;
import nb.scode.tanyasoalmvp.modelretro.LoginResult;
import nb.scode.tanyasoalmvp.modelretro.PackageRetro;
import nb.scode.tanyasoalmvp.modelretro.ProfileDetail;
import nb.scode.tanyasoalmvp.modelretro.ProfileResponse;
import nb.scode.tanyasoalmvp.models.Academics;
import nb.scode.tanyasoalmvp.network.ApiService;
import nb.scode.tanyasoalmvp.util.NetworkManager;
import okhttp3.RequestBody;
import org.json.JSONArray;
import org.json.JSONObject;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by neobyte on 2/19/2017.
 */

public class CloudDataRepo implements CloudDataTask {

  private static final String TAG = CloudDataRepo.class.getSimpleName();
  private final String deviceType = "Android";
  private ApiService apiService;
  private String deviceId = null;
  private String accessToken;
  private Long mBalance;
  private NetworkManager networkManager;
  private Academics academix = null;
  private ProfileDetail profileDetail = null;

  @Inject public CloudDataRepo(ApiService apiService, NetworkManager networkManager) {
    this.apiService = apiService;
    this.networkManager = networkManager;
  }

  @Override public void login(String username, String password, String token,
      final Callback callback) {
    callback.process();
    deviceId = token;
    HashMap<String, String> hashMap = new HashMap<>();
    hashMap.put("email", username);
    hashMap.put("password", password);
    hashMap.put("deviceId", deviceId);
    hashMap.put("deviceType", deviceType);
    JSONObject jsonObject = new JSONObject(hashMap);
    RequestBody requestBody =
        RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
            jsonObject.toString());
    Timber.d("login(): body => " + jsonObject.toString());
    apiService.login(requestBody)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<LoginResult>() {
          @Override public void accept(@NonNull LoginResult loginResult) throws Exception {
            Timber.d("accept(): login");
            if (loginResult.isSuccess()) {
              accessToken = loginResult.getToken();
              callback.success(loginResult.getToken());
            } else {
              callback.failed(loginResult.getMessage());
            }
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(@NonNull Throwable throwable) throws Exception {
            callback.failed(throwable.getMessage());
          }
        });
  }

  @Override public void getPredefines(final Callback callback) {
    callback.process();
    if (academix == null) {
      apiService.getPredefines()
          .observeOn(AndroidSchedulers.mainThread())
          .subscribeOn(Schedulers.io())
          .subscribe(new ResourceObserver<Academics>() {
            @Override public void onNext(Academics academics) {
              academix = academics;
              callback.success(null);
            }

            @Override public void onError(Throwable e) {
              Timber.e("onError getPredefines(): %s", e.getMessage());
              callback.failed(getError(e));
            }

            @Override public void onComplete() {

            }
          });
    } else {
      callback.success(null);
    }
  }

  @Override public Academics getAcademix() {
    return academix;
  }

  @Override public void signup(String email, String username, String pass, String confpass,
      int jnsAkad, int tktAkad, String sekolah, String promo, String namaLkp, String noHp,
      String tgl, final Callback callback) {
    callback.process();

    HashMap<String, Object> map = new HashMap<>();
    map.put("email", email);
    map.put("username", username);
    map.put("password", pass);
    map.put("academic_type_id", jnsAkad);
    map.put("academic_level_id", tktAkad);
    map.put("register_promo_code", promo);
    map.put("school_name", sekolah);
    map.put("first_name", namaLkp);
    map.put("phone_number", noHp);
    map.put("dob", tgl);

    JSONObject jsonObject = new JSONObject(map);

    RequestBody requestBody =
        RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
            (jsonObject).toString());

    Timber.d("signup(): body => " + jsonObject.toString());
    apiService.signUp(requestBody)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new ResourceObserver<BaseApiResponse>() {
          @Override public void onNext(@NonNull BaseApiResponse baseApiResponse) {
            if (baseApiResponse.isSuccess()) {
              //TODO : Succes signup
              callback.success(null);
            } else {
              callback.failed(baseApiResponse.getMessage());
            }
          }

          @Override public void onError(@NonNull Throwable throwable) {
            Log.e(TAG, "accept: error signup " + throwable.getMessage());
            callback.failed(throwable.getMessage());
          }

          @Override public void onComplete() {

          }
        });
  }

  @Override public void lupaPass(String username, String namalkp, String noHp, String tgl,
      final Callback callback) {
    callback.process();
    HashMap<String, String> map = new HashMap<>();
    map.put("username", username);
    map.put("first_name", namalkp);
    map.put("dob", tgl);

    JSONObject jsonObject = new JSONObject(map);

    RequestBody requestBody =
        RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
            (jsonObject).toString());

    Timber.d("lupaPass(): body => " + jsonObject.toString());

    apiService.forgotpwd(requestBody)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<BaseApiResponse>() {
          @Override public void accept(@NonNull BaseApiResponse baseApiResponse) throws Exception {
            if (baseApiResponse.isSuccess()) {
              callback.success(null);
            } else {
              callback.failed(baseApiResponse.getMessage());
            }
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(@NonNull Throwable throwable) throws Exception {
            callback.failed(throwable.getMessage());
          }
        });
  }

  @Override public void resetPass(String username, String pass, String confpass,
      final Callback callback) {
    callback.process();
    HashMap<String, String> map = new HashMap<>();
    map.put("email", username);
    map.put("password", pass);
    map.put("confirm_password", confpass);

    JSONObject jsonObject = new JSONObject(map);

    RequestBody requestBody =
        RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
            (jsonObject).toString());

    Timber.d("resetPass(): body => " + jsonObject.toString());

    apiService.resetpwd(requestBody)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<BaseApiResponse>() {
          @Override public void accept(@NonNull BaseApiResponse baseApiResponse) throws Exception {
            if (baseApiResponse.isSuccess()) {
              callback.success(null);
            } else {
              callback.failed(baseApiResponse.getMessage());
            }
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(@NonNull Throwable throwable) throws Exception {
            callback.failed(throwable.getMessage());
          }
        });
  }

  @Override public void getBalance(final Callback callback) {
    callback.process();
    apiService.getBalance(accessToken)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<BalanceUser>() {
          @Override public void accept(@NonNull BalanceUser balanceUser) throws Exception {
            if(balanceUser.isSuccess()){
              mBalance = balanceUser.getAmount();
            }
            else {
              mBalance = 0L;
            }
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(@NonNull Throwable throwable) throws Exception {
            mBalance = 0L;
            callback.failed(throwable.getMessage());
          }
        });
  }

  @Override public Long getBalanceValue() {
    return mBalance;
  }

  @Override public ProfileDetail getProfileDetail() {
    return profileDetail;
  }

  @Override public void getProfile(String token, final Callback callback) {
    accessToken = token;
    apiService.getProfile(token)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new ResourceObserver<ProfileResponse>() {
          @Override public void onNext(@NonNull ProfileResponse profileResponse) {
            Log.d(TAG, "accept: OK");
            if (profileResponse.isSuccess()) {
              profileDetail = profileResponse.getData();
              callback.success(profileDetail);
            } else {
              callback.failed(profileResponse.getMessage());
            }
          }

          @Override public void onError(@NonNull Throwable throwable) {
            Log.e(TAG, "accept: ERROR getProfile" + throwable.getMessage());
            callback.failed("Error user");
          }

          @Override public void onComplete() {

          }
        });
  }

  @Override public void changeProfile(String email, String firstName, String phoneNumber,
      String dob, int academicType, int academicLevel, String schoolName, final Callback callback) {
    callback.process();
    HashMap<String, Object> map = new HashMap<>();
    map.put("email", email);
    map.put("first_name", firstName);
    map.put("phone_number", phoneNumber);
    map.put("dob", dob);
    map.put("academic_type_id", academicType);
    map.put("academic_level_id", academicLevel);
    map.put("school_name", schoolName);

    JSONObject jsonObject = new JSONObject(map);
    RequestBody requestBody =
        RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
            (jsonObject).toString());

    Timber.d("changeProfile(): body => " + jsonObject.toString());

    apiService.setProfile(accessToken, requestBody)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<ProfileResponse>() {
          @Override public void accept(@NonNull ProfileResponse profileResponse) throws Exception {
            callback.success(null);
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(@NonNull Throwable throwable) throws Exception {
            callback.failed(throwable.getMessage());
          }
        });
  }

  @Override public void getPackageTanya(final Callback callback) {
    callback.process();
    apiService.getPackageTanya()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<JSONObject>() {
          @Override public void accept(@NonNull JSONObject object) throws Exception {
            JSONArray array = object.getJSONArray("packages");
            if (array != null) {
              Type token = new TypeToken<List<PackageRetro>>(){}.getType();
              Gson gson = new Gson();
              List<PackageRetro> packageRetroList = gson.fromJson(array.toString(), token);
              callback.success(packageRetroList);
            }
            else {
              Log.e(TAG, "accept: array is null");
            }
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(@NonNull Throwable throwable) throws Exception {

          }
        });
  }

  private String getError(Throwable t) {

    if (t instanceof SocketTimeoutException) {
      return "Request Time Out";
    } else if (t instanceof HttpException) {
      return "Http Error";
    } else {
      return "Unknwon Error";
    }
  }
}
