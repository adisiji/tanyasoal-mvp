package nb.scode.tanyasoalmvp.data.clouddata;

import nb.scode.tanyasoalmvp.modelretro.ProfileDetail;
import nb.scode.tanyasoalmvp.models.Academics;

/**
 * Created by neobyte on 2/19/2017.
 */

public interface CloudDataTask {

  void login(String email, String password, String token, Callback callback);

  void getPredefines(Callback callback);

  Academics getAcademix();

  void signup(String email, String username, String pass, String confpass, int jnsAkad, int tktAkad,
      String sekolah, String promo, String namaLkp, String noHp, String tgl, Callback callback);

  void lupaPass(String email, String namalkp, String noHp, String tgl, Callback callback);

  void resetPass(String email, String pass, String confpass, Callback callback);

  void getBalance(Callback callback);

  Long getBalanceValue();

  void getProfile(String token, Callback callback);

  ProfileDetail getProfileDetail();

  void getPackageTanya(Callback callback);

  void changeProfile(String email, String firstName, String phoneNumber, String dob,
      int academicType, int academicLevel, String schoolName, Callback callback);



  interface Callback {

    void process();

    void success(Object object);

    void failed(String msg);
  }
}
