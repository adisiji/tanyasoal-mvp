package nb.scode.tanyasoalmvp.modelretro;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neobyte on 2/20/2017.
 */

public class LoginResult {

  @SerializedName("isSuccess") @Expose private boolean isSuccess;

  @SerializedName("message") @Expose private String message;

  @SerializedName("token") @Expose private String token;

  public boolean isSuccess() {
    return isSuccess;
  }

  public void setSuccess(boolean success) {
    isSuccess = success;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
