
package nb.scode.tanyasoalmvp.modelretro;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileDetail {

  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("school")
  @Expose
  private String school;
  @SerializedName("academicLevel")
  @Expose
  private Integer academicLevel;
  @SerializedName("academicType")
  @Expose
  private Integer academicType;
  @SerializedName("status")
  @Expose
  private Integer status;

  @SerializedName("access_token")
  @Expose
  private String accessToken;

  @SerializedName("trial")
  @Expose
  private Integer trial;
  @SerializedName("trialDate")
  @Expose
  private Object trialDate;
  @SerializedName("fullname")
  @Expose
  private Object fullname;
  @SerializedName("phonenumber")
  @Expose
  private String phonenumber;
  @SerializedName("dateofbirth")
  @Expose
  private String dateofbirth;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSchool() {
    return school;
  }

  public void setSchool(String school) {
    this.school = school;
  }

  public Integer getAcademicLevel() {
    return academicLevel;
  }

  public void setAcademicLevel(Integer academicLevel) {
    this.academicLevel = academicLevel;
  }

  public Integer getAcademicType() {
    return academicType;
  }

  public void setAcademicType(Integer academicType) {
    this.academicType = academicType;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public Integer getTrial() {
    return trial;
  }

  public void setTrial(Integer trial) {
    this.trial = trial;
  }

  public Object getTrialDate() {
    return trialDate;
  }

  public void setTrialDate(Object trialDate) {
    this.trialDate = trialDate;
  }

  public Object getFullname() {
    return fullname;
  }

  public void setFullname(Object fullname) {
    this.fullname = fullname;
  }

  public String getPhonenumber() {
    return phonenumber;
  }

  public void setPhonenumber(String phonenumber) {
    this.phonenumber = phonenumber;
  }

  public String getDateofbirth() {
    return dateofbirth;
  }

  public void setDateofbirth(String dateofbirth) {
    this.dateofbirth = dateofbirth;
  }
}
