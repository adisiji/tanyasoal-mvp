package nb.scode.tanyasoalmvp.modelretro;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neobyte on 2/13/2017.
 */

public class ProfileResponse {
  @SerializedName("isSuccess") @Expose private boolean isSuccess;

  @SerializedName("message") @Expose private String message;

  private ProfileDetail data;

  public boolean isSuccess() {
    return isSuccess;
  }

  public void setSuccess(boolean success) {
    isSuccess = success;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ProfileDetail getData() {
    return data;
  }

  public void setData(ProfileDetail data) {
    this.data = data;
  }
}
