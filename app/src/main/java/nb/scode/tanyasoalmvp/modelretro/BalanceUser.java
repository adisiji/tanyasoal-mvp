package nb.scode.tanyasoalmvp.modelretro;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neobyte on 8/7/2017.
 */

public class BalanceUser {
  @SerializedName("isSuccess") @Expose private boolean isSuccess;
  @SerializedName("message") @Expose private String message;
  @SerializedName("amount") @Expose private Long amount;

  public boolean isSuccess() {
    return isSuccess;
  }

  public void setSuccess(boolean success) {
    isSuccess = success;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }
}
