package nb.scode.tanyasoalmvp.modelretro;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neobyte on 2/13/2017.
 */

public class PackageRetro {

    @SerializedName("i")
    @Expose
    private Integer id;
    @SerializedName("n")
    @Expose
    private String name;
    @SerializedName("p")
    @Expose
    private Integer price;
    @SerializedName("a")
    @Expose
    private Integer interval;
    @SerializedName("v")
    @Expose
    private Integer video;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getVideo() {
        return video;
    }

    public void setVideo(Integer video) {
        this.video = video;
    }
}
