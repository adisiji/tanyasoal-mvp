package nb.scode.tanyasoalmvp.modelretro;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neobyte on 2/18/2017.
 */

public class ConversationRetro {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("lesson_id")
    @Expose
    private Integer lessonId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("number_of_question")
    @Expose
    private Integer numberOfQuestion;
    @SerializedName("is_private")
    @Expose
    private Integer isPrivate;
    @SerializedName("closed")
    @Expose
    private Integer closed;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("attachment")
    @Expose
    private AttachmentRootRetro attachment;
    @SerializedName("comments")
    @Expose
    private List<CommentRetro> comments = null;

}
