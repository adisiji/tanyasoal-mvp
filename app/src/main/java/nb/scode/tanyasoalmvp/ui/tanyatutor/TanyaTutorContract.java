package nb.scode.tanyasoalmvp.ui.tanyatutor;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 3/4/2017.
 */

public interface TanyaTutorContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView<Presenter> {


    }

}
