package nb.scode.tanyasoalmvp.ui.tanyatutor;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;
import nb.scode.tanyasoalmvp.ui.layanantanyatutor.LayananTanyaTutorActivity;

public class TanyaTutorActivity extends BaseActivity implements TanyaTutorContract.View {

    @Inject
    TanyaTutorPresenter mPresenter;
    private boolean defBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tanya_tutor);
        defBack = getIntent().getBooleanExtra(FROM_SUCCESS,false);
        DaggerTanyaTutorComponent.builder().dataComponent(App.getDataComponent())
                .tanyaTutorModule(new TanyaTutorModule(this))
                .build().inject(this);
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(), msg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override public void onBackPressed() {
      super.onBackPressed();
      if(defBack){
        Intent i = new Intent(getApplicationContext(), LayananTanyaTutorActivity.class);
        i.putExtra(FROM_SUCCESS,true);
        startActivity(i);
      }
    }
}
