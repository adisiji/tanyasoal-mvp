package nb.scode.tanyasoalmvp.ui.layanantanyatutor;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;

/**
 * Created by neobyte on 3/1/2017.
 */

public class LayananTanyaTutorPresenter implements LayananTanyaTutorContract.Presenter {

    private LayananTanyaTutorContract.View mView;
    private CloudDataTask mDataTask;

    @Inject
    public LayananTanyaTutorPresenter(LayananTanyaTutorContract.View mView, CloudDataRepo dataTask) {
        this.mView = mView;
        this.mDataTask = dataTask;
    }

    @Override
    public void start() {

    }
}
