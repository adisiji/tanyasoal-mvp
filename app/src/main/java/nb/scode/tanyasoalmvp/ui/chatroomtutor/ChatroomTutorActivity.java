package nb.scode.tanyasoalmvp.ui.chatroomtutor;

import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;

public class ChatroomTutorActivity extends BaseActivity implements ChatroomTutorContract.View {

    @Inject
    ChatroomTutorPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatroom_tutor);
        DaggerChatroomTutorComponent.builder()
                .dataComponent(App.getDataComponent())
                .chatroomTutorPresenterModule(new ChatroomTutorPresenterModule(this))
                .build().inject(this);
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
    }
}
