package nb.scode.tanyasoalmvp.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import custom_font.EditTextMyriad;
import custom_font.TextViewMyriad;
import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseFirstActivity;
import nb.scode.tanyasoalmvp.ui.homesiswa.HomeSiswaActivity;
import nb.scode.tanyasoalmvp.ui.lupapass.LupaPassActivity;
import nb.scode.tanyasoalmvp.ui.signup.SignupActivity;

public class LoginActivity extends BaseFirstActivity implements LoginContract.View {

    @Inject
    LoginPresenter mPresenter;

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.btn_daftar)
    TextViewMyriad btnCreate;
    @BindView(R.id.myEditText)
    EditTextMyriad etEmail;
    @BindView(R.id.myEditText2)
    EditTextMyriad etPass;
    @BindString(R.string.error_field_empty)
    String empty;

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        isHome = true;
        /*Inject component*/
        DaggerLoginComponent.builder().dataComponent(App.getDataComponent())
                .loginPresenterModule(new LoginPresenterModule(this))
                .build().inject(this);
        mPresenter.start();
        /*Load logo*/
        Glide.with(this)
                .load(R.drawable.logo_fix)
                .into(logo);
        /*setting toolbar icon*/
        getBack().setVisibility(View.GONE);
        getProfile().setVisibility(View.GONE);
        getMail().setVisibility(View.GONE);
    }

    @Override
    public void gotoHome() {
        Intent i = new Intent(getApplicationContext(), HomeSiswaActivity.class);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.btn_login)
    void login(){
        String email = etEmail.getText().toString().trim();
        String pass = etPass.getText().toString().trim();
        mPresenter.login(email, pass);
    }

    @OnClick(R.id.btn_daftar)
    void daftar(){
        Intent i = new Intent(getApplicationContext(), SignupActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.forgotpwd)
    void forgotPass(){
        Intent i = new Intent(getApplicationContext(), LupaPassActivity.class);
        startActivity(i);
    }

    @Override
    public void delError() {
        etEmail.setError(null);
        etPass.setError(null);
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void errorPass() {
        etPass.setError(getString(R.string.error_password));
    }

    /*
    @Override
    public void errorMail() {
        etEmail.setError(getString(R.string.error_email));
    }
     */

    @Override
    public void errorEmptyPass() {
        etPass.setError(empty);
    }

    @Override
    public void errorEmptyUsername() {
        etEmail.setError(empty);
    }

}
