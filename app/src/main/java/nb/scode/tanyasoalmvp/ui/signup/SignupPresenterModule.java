package nb.scode.tanyasoalmvp.ui.signup;

import dagger.Module;
import dagger.Provides;

/**
 * Created by neobyte on 2/28/2017.
 */

@Module
public class SignupPresenterModule {

    private SignupContract.View view;

    public SignupPresenterModule(SignupContract.View view) {
        this.view = view;
    }

    @Provides
    SignupContract.View providesSignupView(){
        return view;
    }
}
