package nb.scode.tanyasoalmvp.ui.isivoucher;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/9/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = IsiVoucherModule.class)
public interface IsiVoucherComponent {
    void inject(IsiVoucherActivity activity);
}
