package nb.scode.tanyasoalmvp.ui.homesiswa;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 2/19/2017.
 */

public interface HomeSiswaContract {

    interface Presenter extends BasePresenter {

        void getBalance();

        void getProfileDetail();

        void setLayout();

    }

    interface View extends BaseView<Presenter> {

        void setBalance(Long value);

        void setTitleName(String content);

    }

}
