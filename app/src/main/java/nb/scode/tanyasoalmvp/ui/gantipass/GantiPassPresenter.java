package nb.scode.tanyasoalmvp.ui.gantipass;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;
import nb.scode.tanyasoalmvp.util.Helper;

/**
 * Created by neobyte on 2/28/2017.
 */

public class GantiPassPresenter implements GantiPassContract.Presenter {

    private GantiPassContract.View view;
    private CloudDataTask dataTask;
    private Helper helper;

    @Inject
    public GantiPassPresenter(GantiPassContract.View view, CloudDataRepo dataTask) {
        this.view = view;
        this.dataTask = dataTask;
        helper = new Helper();
    }

    @Override
    public void start() {

    }

    @Override
    public void sendForm(String email, String pass, String confPass) {
        dataTask.resetPass(email, pass, confPass, new CloudDataTask.Callback() {
            @Override
            public void process() {
                view.showLoading();
            }

            @Override
            public void success(Object msg) {
                view.hideLoading();
                //TODO : After reset, where will it go ?
            }

            @Override
            public void failed(String msg) {
                view.hideLoading();
                view.showError(msg);
            }
        });
    }

    @Override
    public boolean cekForm(String pass, String confpass) {
        if(pass.length()==0){
            view.errorEmptyPass();
            return false;
        }
        else if(pass.length()<6) {
            view.errorMin6Pass();
            return false;
        }
        else if(!helper.isValidPassword(pass)) {
            view.errorPass();
            return false;
        }
        if(confpass.length()==0) {
            view.errorEmptyConfPass();
            return false;
        }
        else if(!confpass.equals(pass)){
            view.errorConfPass();
            return false;
        }
        return true;
    }
}
