package nb.scode.tanyasoalmvp.ui.isisaldo;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/2/2017.
 */

@Module
public class IsiSaldoPresenterModule {

    private IsiSaldoContract.View mView;

    public IsiSaldoPresenterModule(IsiSaldoContract.View view) {
        mView = view;
    }

    @ActScoped
    @Provides
    IsiSaldoContract.View providesIsiSaldoView(){
        return mView;
    }
}
