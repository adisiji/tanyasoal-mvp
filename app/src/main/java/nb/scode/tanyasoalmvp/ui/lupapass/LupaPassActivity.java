package nb.scode.tanyasoalmvp.ui.lupapass;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import custom_font.EditTextMyriad;
import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseFirstActivity;
import nb.scode.tanyasoalmvp.ui.gantipass.GantiPassActivity;
import nb.scode.tanyasoalmvp.util.DateTextWatcher;

public class LupaPassActivity extends BaseFirstActivity implements LupaPassContract.View {

    @Inject
    LupaPassPresenter presenter;

    @BindView(R.id.txt_lp_email)
    EditTextMyriad et_username;
    @BindView(R.id.txt_lp_nama_lkp)
    EditTextMyriad et_first_name;
    @BindView(R.id.txt_lp_no_hp)
    EditTextMyriad et_hp;
    @BindView(R.id.lp_tgl_lhr)
    EditTextMyriad et_tlh;
    @BindString(R.string.error_field_empty)
    String emptyfield;
    @BindString(R.string.error_wrong_format)
    String wrongFormat;

    String username, first_name, phone, bod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_pass);
        DateTextWatcher dateTextWatcher = new DateTextWatcher(et_tlh);
        et_tlh.addTextChangedListener(dateTextWatcher);
        DaggerLupaPassComponent.builder().dataComponent(App.getDataComponent())
                .lupaPassPresenterModule(new LupaPassPresenterModule(this))
                .build().inject(this);
    }

    @OnClick(R.id.btn_kirim_sandi)
    void kirimSandi(){
        username = et_username.getText().toString();
        first_name = et_first_name.getText().toString();
        phone = et_hp.getText().toString();
        bod = et_tlh.getText().toString();

        presenter.sendForm(username, first_name, phone, bod);

    }

    @Override
    public void successLupaPass() {
        Intent i = new Intent(getApplicationContext(), GantiPassActivity.class);
        i.putExtra("username", username);
        startActivity(i);
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void erorHp() {
        et_hp.setError(wrongFormat);
    }

    @Override
    public void errorEmptyHp() {
        et_hp.setError(emptyfield);
    }

    @Override
    public void errorEmptyNamaLkp() {
        et_first_name.setError(emptyfield);
    }

    @Override
    public void errorMin2NamaLkp() {
        et_first_name.setError(getString(R.string.error_min_2_char));
    }

    @Override
    public void errorEmptyMail() {
        et_username.setError(emptyfield);
    }

    @Override
    public void delError() {
        et_username.setError(null);
        et_first_name.setError(null);
        et_hp.setError(null);
        et_tlh.setError(null);
    }
}
