package nb.scode.tanyasoalmvp.ui.loguser;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;

/**
 * Created by neobyte on 3/2/2017.
 */

public class LogUserPresenter implements LogUserConract.Presenter {

    private final LogUserConract.View mView;
    private final CloudDataTask dataTask;

    @Inject
    public LogUserPresenter(LogUserConract.View mView, CloudDataRepo dataTask) {
        this.mView = mView;
        this.dataTask = dataTask;
    }

    @Override
    public void start() {

    }
}
