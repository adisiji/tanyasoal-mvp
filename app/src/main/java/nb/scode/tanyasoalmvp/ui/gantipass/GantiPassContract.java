package nb.scode.tanyasoalmvp.ui.gantipass;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 2/28/2017.
 */

public interface GantiPassContract {
    interface Presenter extends BasePresenter {
        boolean cekForm(String pass, String confPass);

        void sendForm(String email, String pass, String confPass);
    }

    interface View extends BaseView<Presenter>{

        void errorEmptyPass();

        void errorMin6Pass();

        void errorPass();

        void errorEmptyConfPass();

        void errorConfPass();

    }
}
