package nb.scode.tanyasoalmvp.ui.chattanyatutor;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/1/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = ChatTanyaTutorPresenterModule.class)
public interface ChatTanyaTutorComponent {
    void inject(ChatroomTanyaTutorActivity chatroomTanyaTutorActivity);
}
