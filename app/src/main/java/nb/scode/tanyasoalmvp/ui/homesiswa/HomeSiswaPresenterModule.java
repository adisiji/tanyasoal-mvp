package nb.scode.tanyasoalmvp.ui.homesiswa;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 2/19/2017.
 */

@Module
public class HomeSiswaPresenterModule {

    private HomeSiswaContract.View view;

    public HomeSiswaPresenterModule(HomeSiswaContract.View view) {
        this.view = view;
    }

    @Provides
    @ActScoped
    public HomeSiswaContract.View providesHomeView(){
        return this.view;
    }
}
