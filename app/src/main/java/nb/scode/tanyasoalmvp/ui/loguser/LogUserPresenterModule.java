package nb.scode.tanyasoalmvp.ui.loguser;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/2/2017.
 */

@Module
public class LogUserPresenterModule {

    private LogUserConract.View view;

    public LogUserPresenterModule(LogUserConract.View view) {
        this.view = view;
    }

    @ActScoped
    @Provides
    LogUserConract.View providesLogUserView(){
        return view;
    }

}
