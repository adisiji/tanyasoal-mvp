package nb.scode.tanyasoalmvp.ui.login;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 2/19/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = {LoginPresenterModule.class})

public interface LoginComponent {

    void inject(LoginActivity loginActivity);

}
