package nb.scode.tanyasoalmvp.ui.availablequestion;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 3/2/2017.
 */

public interface AvailQuestContract {

    interface Presenter extends BasePresenter {


    }

    interface View extends BaseView<Presenter>{


    }

}
