package nb.scode.tanyasoalmvp.ui.login;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 2/19/2017.
 */

@Module
public class LoginPresenterModule {

    private LoginContract.View view;

    public LoginPresenterModule(LoginContract.View view){
        this.view = view;
    }

    @Provides
    @ActScoped
    public LoginContract.View providesLoginView(){
        return view;
    }

}
