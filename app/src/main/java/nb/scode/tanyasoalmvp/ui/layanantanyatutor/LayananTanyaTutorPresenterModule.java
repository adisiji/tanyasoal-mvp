package nb.scode.tanyasoalmvp.ui.layanantanyatutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by neobyte on 3/1/2017.
 */

@Module
public class LayananTanyaTutorPresenterModule {

    private LayananTanyaTutorContract.View mView;

    public LayananTanyaTutorPresenterModule(LayananTanyaTutorContract.View mView) {
        this.mView = mView;
    }

    @Provides
    public LayananTanyaTutorContract.View providesLayTanyaView(){
        return mView;
    }
}
