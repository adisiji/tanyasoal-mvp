package nb.scode.tanyasoalmvp.ui.isisaldo;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/2/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = IsiSaldoPresenterModule.class)
public interface IsiSaldoComponent {
    void inject(IsiSaldoActivity isiSaldoActivity);
}
