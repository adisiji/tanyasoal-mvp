package nb.scode.tanyasoalmvp.ui.layanantanyatutor;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 3/1/2017.
 */

public interface LayananTanyaTutorContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView<Presenter> {

    }

}
