package nb.scode.tanyasoalmvp.ui.signup;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 2/28/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = SignupPresenterModule.class)
public interface SignupComponent {

    void inject(SignupActivity signupActivity);

}
