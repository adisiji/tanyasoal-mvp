package nb.scode.tanyasoalmvp.ui.signup;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import com.jaredrummler.materialspinner.MaterialSpinner;
import custom_font.EditTextMyriad;
import es.dmoral.toasty.Toasty;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.models.AcademicLevels;
import nb.scode.tanyasoalmvp.models.AcademicTypes;
import nb.scode.tanyasoalmvp.models.Academics;
import nb.scode.tanyasoalmvp.ui.baseact.BaseFirstActivity;
import nb.scode.tanyasoalmvp.ui.suksesreg.SuksesRegActivity;
import nb.scode.tanyasoalmvp.util.DateTextWatcher;

public class SignupActivity extends BaseFirstActivity implements SignupContract.View {

  @Inject SignupPresenter presenter;

  @BindView(R.id.txt_create_jenakad) MaterialSpinner spinJenAkad;
  @BindView(R.id.spin_tingkat_akad) MaterialSpinner spinTktAkad;
  @BindView(R.id.txt_create_email) EditTextMyriad tv_email;
  @BindView(R.id.txt_create_nama) EditTextMyriad tv_nama;
  @BindView(R.id.txt_create_password) EditTextMyriad tv_pass;
  @BindView(R.id.txt_create_confpass) EditTextMyriad tv_confpass;
  @BindView(R.id.txt_create_sekolah) EditTextMyriad tv_sekolah;
  @BindView(R.id.txt_kd_promo) EditTextMyriad tv_promo;
  @BindView(R.id.txt_nama_lkp) EditTextMyriad tv_nama_lkp;
  @BindView(R.id.txt_no_hp) EditTextMyriad tv_no_hp;
  @BindView(R.id.daftar_tgl_lhr) EditTextMyriad tv_tgl;

  @BindString(R.string.error_email_taken) String emailTaken;
  @BindString(R.string.error_login_taken) String nameTaken;
  @BindString(R.string.error_field_empty) String emptyfield;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_signup);
    DateTextWatcher dateTextWatcher = new DateTextWatcher(tv_tgl);
    tv_tgl.addTextChangedListener(dateTextWatcher);
    DaggerSignupComponent.builder()
        .dataComponent(App.getDataComponent())
        .signupPresenterModule(new SignupPresenterModule(this))
        .build()
        .inject(this);
    presenter.start();
  }

  @Override public void setSpinner(Academics academics) {
    List<String> typeName = new ArrayList<>();
    for (AcademicTypes academicTypes : academics.getAcademicTypeList()) {
      typeName.add(academicTypes.getName());
    }
    List<String> levelName = new ArrayList<>();
    for (AcademicLevels academicLevels : academics.getAcademicLevelList()) {
      levelName.add(academicLevels.getName());
    }
    spinJenAkad.setItems(typeName);
    spinTktAkad.setItems(levelName);
  }

  @Override public void showLoading() {
    mDialog.show();
  }

  @Override public void hideLoading() {
    mDialog.dismiss();
  }

  @OnClick(R.id.btn_daftar) void daftar() {
    String nama, email, pass, confpass, sekolah, promo, namalkp, hp, tgl;
    int jenakad, tktakad;

    email = tv_email.getText().toString().trim();
    nama = tv_nama.getText().toString().trim();
    pass = tv_pass.getText().toString().trim();
    confpass = tv_confpass.getText().toString().trim();
    jenakad = spinJenAkad.getSelectedIndex();
    tktakad = spinTktAkad.getSelectedIndex();
    sekolah = tv_sekolah.getText().toString().trim();
    promo = tv_promo.getText().toString().trim();
    namalkp = tv_nama_lkp.getText().toString().trim();
    hp = tv_no_hp.getText().toString().trim();
    tgl = tv_tgl.getText().toString().trim();

    presenter.signup(email, nama, pass, confpass, jenakad, tktakad, sekolah, promo, namalkp, hp,
        tgl);
  }

  @Override public void showError(String msg) {
    Toasty.error(getApplicationContext(), msg, Toast.LENGTH_SHORT, true).show();
  }

  @Override public void erorHp() {
    tv_no_hp.setError(getString(R.string.error_phone));
  }

  @Override public void errorEmptyHp() {
    tv_no_hp.setError(emptyfield);
  }

  @Override public void errorMin2NamaLkp() {
    tv_nama_lkp.setError(getString(R.string.error_min_2_char));
  }

  @Override public void errorEmptyNamaLkp() {
    tv_nama_lkp.setError(emptyfield);
  }

  @Override public void errorPromo() {

  }

  @Override public void errorMin5Sekolah() {
    tv_sekolah.setError(getString(R.string.error_min_5_char));
  }

  @Override public void errorEmptySekolah() {
    tv_sekolah.setError(emptyfield);
  }

  @Override public void errorConfPass() {
    tv_confpass.setError(getString(R.string.error_wrong_format));
  }

  @Override public void errorEmptyConfPass() {
    tv_confpass.setError(emptyfield);
  }

  @Override public void errorPass() {
    tv_pass.setError(getString(R.string.error_wrong_format));
  }

  @Override public void errorMin6Pass() {
    tv_pass.setError(getString(R.string.error_min_6_char));
  }

  @Override public void errorEmptyPass() {
    tv_pass.setError(emptyfield);
  }

  @Override public void errorUsername() {
    tv_nama.setError(getString(R.string.error_wrong_format));
  }

  @Override public void errorMin5Username() {
    tv_nama.setError(getString(R.string.error_min_5_char));
  }

  @Override public void errorEmptyUsername() {
    tv_nama.setError(emptyfield);
  }

  @Override public void errorMail() {
    tv_email.setError(getString(R.string.error_wrong_format));
  }

  @Override public void errorEmptyMail() {
    tv_email.setError(emptyfield);
  }

  @Override public void delError() {
    tv_nama.setError(null);
    tv_nama_lkp.setError(null);
    tv_pass.setError(null);
    tv_confpass.setError(null);
    tv_email.setError(null);
    tv_sekolah.setError(null);
    tv_no_hp.setError(null);
  }

  @Override public void backToLogin() {
    onBackPressed();
  }

  @Override public void successSignUp() {
    Intent intent = new Intent(getApplicationContext(), SuksesRegActivity.class);
    startActivity(intent);
    finish();
  }
}
