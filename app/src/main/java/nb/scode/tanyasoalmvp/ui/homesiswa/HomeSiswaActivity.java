package nb.scode.tanyasoalmvp.ui.homesiswa;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import custom_font.TextViewApple;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;
import nb.scode.tanyasoalmvp.ui.isisaldo.IsiSaldoActivity;
import nb.scode.tanyasoalmvp.ui.komunitas.KomunitasActivity;
import nb.scode.tanyasoalmvp.ui.layanantanyatutor.LayananTanyaTutorActivity;
import nb.scode.tanyasoalmvp.ui.login.LoginActivity;
import nb.scode.tanyasoalmvp.ui.loguser.LogUserActivity;

public class HomeSiswaActivity extends BaseActivity implements HomeSiswaContract.View{

    @Inject
    HomeSiswaPresenter mPresenter;

  @BindView(R.id.title_home) TextViewApple tvTitleName;
  @BindView(R.id.tv_saldo_home)
  TextViewApple tvSaldo;
  @BindView(R.id.tv_layanan_home)
  TextViewApple tvLayanan;

    private boolean isDoubleBackToExit = false;

    /*
     * Loading -> GetBalance -> Get Active Package -> Dismiss Loading
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_siswa);
        DaggerHomeSiswaComponent.builder().dataComponent(App.getDataComponent())
                .homeSiswaPresenterModule(new HomeSiswaPresenterModule(this))
                .build().inject(this);
        isHome = true;
        getBackToolbar().setVisibility(View.GONE);
        mPresenter.start();
    }

    @OnClick(R.id.btn_tny_tutor)
    void tanyaTutor(){
        Intent i = new Intent(getApplicationContext(),LayananTanyaTutorActivity.class);
        startActivity(i);
    }

    /*
    @OnClick(R.id.btn_paketsedia)
    void paketSedia(){
        Intent i = new Intent(getApplicationContext(), PaketSediaActivity.class);
        startActivity(i);
    }
    */

    @OnClick(R.id.btn_komunitas)
    void keKomunitas(){
        Intent i = new Intent(getApplicationContext(), KomunitasActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btn_isiSaldo)
    void isiSaldo(){
        Intent i = new Intent(getApplicationContext(), IsiSaldoActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.cat_aktivitas_home)
    void catAktivitas(){
        Intent i = new Intent(getApplicationContext(), LogUserActivity.class);
        startActivity(i);
    }


    @Override
    public void onBackPressed() {

        if (drawerFragment.isDrawerOpen()) {

            drawerFragment.closeDrawer();

        } else {

            if (isDoubleBackToExit) {
                super.onBackPressed();
              Intent homeIntent= new Intent(Intent.ACTION_MAIN);
              homeIntent.addCategory(Intent.CATEGORY_HOME);
              homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              startActivity(homeIntent);
              finish();
            }
            if (!isDoubleBackToExit) {
                Toast.makeText(this, getString(R.string.tap_exit), Toast.LENGTH_SHORT).show();
            }
            this.isDoubleBackToExit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isDoubleBackToExit = false;
                }
            }, 2000); //delay 2 detik
        }
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showError(String msg) {

    }

    @Override
    public void setBalance(Long value) {
        tvSaldo.setText("Saldo Anda: Rp"+String.valueOf(value));
    }

    @Override public void setTitleName(String content) {
      tvTitleName.setText(getString(R.string.selamat_datang)+" "+content);
    }
}
