package nb.scode.tanyasoalmvp.ui.chattanyatutor;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 3/1/2017.
 */

public interface ChatTanyaTutorContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView<Presenter> {

    }

}
