package nb.scode.tanyasoalmvp.ui.homesiswa;


import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataRepo;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataTask;

/**
 * Created by neobyte on 2/19/2017.
 */

public class HomeSiswaPresenter implements HomeSiswaContract.Presenter {

    private final CloudDataTask dataTask;
    private final PrefDataTask prefDataRepo;
    private final HomeSiswaContract.View view;

    @Inject
    public HomeSiswaPresenter(CloudDataRepo dataTask, HomeSiswaContract.View view, PrefDataRepo dataRepo) {
        this.dataTask = dataTask;
        this.view = view;
        this.prefDataRepo = dataRepo;
    }

    @Override
    public void start() {
      getProfileDetail();
      getBalance();
    }

  @Override public void getBalance() {
    dataTask.getBalance(new CloudDataTask.Callback() {
      @Override public void process() {

      }

      @Override public void success(Object object) {
        view.setBalance((Long)object);
      }

      @Override public void failed(String msg) {

      }
    });
  }

  @Override public void getProfileDetail() {
      if(dataTask.getProfileDetail() == null) {
        dataTask.getProfile(prefDataRepo.getAccessToken(), new CloudDataTask.Callback() {
          @Override public void process() {
            view.showLoading();
          }

          @Override public void success(Object object) {
            view.hideLoading();
            setLayout();
          }

          @Override public void failed(String msg) {
            view.hideLoading();
            view.showError(msg);
          }
        });
      }
      else { //profileDetail is ready
        setLayout();
      }

    }

  @Override public void setLayout() {
    view.setTitleName(dataTask.getProfileDetail().getName());
  }
}
