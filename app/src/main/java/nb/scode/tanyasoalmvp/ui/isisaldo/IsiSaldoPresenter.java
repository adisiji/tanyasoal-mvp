package nb.scode.tanyasoalmvp.ui.isisaldo;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;

/**
 * Created by neobyte on 3/2/2017.
 */

public class IsiSaldoPresenter implements IsiSaldoContract.Presenter {

    private final CloudDataTask dataTask;
    private final IsiSaldoContract.View mView;

    @Inject
    public IsiSaldoPresenter(CloudDataRepo dataTask, IsiSaldoContract.View view) {

        this.dataTask = dataTask;
        mView = view;

    }

    @Override
    public void start() {

    }
}
