package nb.scode.tanyasoalmvp.ui.signup;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataRepo;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataTask;
import nb.scode.tanyasoalmvp.models.Academics;
import nb.scode.tanyasoalmvp.util.Helper;
import timber.log.Timber;

/**
 * Created by neobyte on 2/28/2017.
 */

public class SignupPresenter implements SignupContract.Presenter {

  private SignupContract.View view;
  private final CloudDataTask dataTask;
  private final PrefDataTask prefDataRepo;
  private Helper helper;

    @Inject
    public SignupPresenter(SignupContract.View view, CloudDataRepo dataTask, PrefDataRepo dataRepo) {
      this.view = view;
      this.dataTask = dataTask;
      this.prefDataRepo = dataRepo;
      helper = new Helper();
    }

    @Override
    public void start() {
        setupAcademix();
    }

    @Override
    public void setupAcademix() {
        dataTask.getPredefines(new CloudDataTask.Callback() {
            @Override
            public void process() {
                view.showLoading();
            }

            @Override
            public void success(Object msg) {
                view.setSpinner(getAcademix());
                view.hideLoading();
            }

            @Override
            public void failed(String msg) {
              view.hideLoading();
              view.backToLogin();
              view.showError(msg);
            }
        });
    }

    @Override
    public Academics getAcademix() {
        return dataTask.getAcademix();
    }

    @Override
    public void signup(final String email,
                       final String username,
                       final String pass,
                       String confpass,
                       int jnsAkad,
                       int tktAkad,
                       String sekolah,
                       String promo,
                       String namaLkp,
                       String noHp,
                       String tgl) {

        view.delError();
        int i = 0;
        if(email.length()==0){
            view.errorEmptyMail();
            i++;
        }
        else if (!helper.isValidEmail(email)) {
            view.errorMail();
            i++;
        }
        if (username.length() == 0) {
            view.errorEmptyUsername();
            i++;
        }
        else if (username.length() < 5) {
            view.errorMin5Username();
            i++;
        }
        else if (!helper.isValidLogin(username)) {
            view.errorUsername();
            i++;
        }
        if(pass.length()==0){
            view.errorEmptyPass();
            i++;
        }
        else if(pass.length()<6) {
            view.errorMin6Pass();
            i++;
        }
        else if(!helper.isValidPassword(pass)) {
            view.errorPass();
            i++;
        }
        if(confpass.length()==0) {
            view.errorEmptyConfPass();
            i++;
        }
        else if(!confpass.equals(pass)){
            view.errorConfPass();
            i++;
        }
        if(sekolah.length() == 0){
            view.errorEmptySekolah();
            i++;
        }
        else if(sekolah.length() < 5){
            view.errorMin5Sekolah();
            i++;
        }
        if (namaLkp.length() == 0){
            view.errorEmptyNamaLkp();
            i++;
        }
        else if(namaLkp.length() < 2) {
            view.errorMin2NamaLkp();
            i++;
        }
        if (noHp.length() == 0){
            view.errorEmptyHp();
            i++;
        }
        else if(!helper.isValidPhone(noHp)){
            view.erorHp();
            i++;
        }
        if(i==0){
          dataTask.signup(email, username, pass, confpass,
              jnsAkad, tktAkad, sekolah, promo, namaLkp, noHp, tgl,
              new CloudDataTask.Callback() {
                @Override
                public void process() {
                  view.showLoading();
                }

                @Override
                public void success(Object msg) { //sukses signup -> goto suksess page
                  Timber.i("success(): signup");
                  dataTask.login(username, pass, prefDataRepo.getFcmToken(), new CloudDataTask.Callback() {
                    @Override public void process() {
                      Timber.i("process(): login from signup");
                    }

                    @Override public void success(Object object) {
                      prefDataRepo.saveAcccessToken((String)object);
                      view.hideLoading();
                      view.successSignUp();
                    }

                    @Override public void failed(String msg) {
                      view.hideLoading();
                      view.showError(msg);
                    }
                  });
                }

                @Override
                public void failed(String msg) {
                  view.hideLoading();
                  view.showError(msg);
                }
              });
        }
    }
}
