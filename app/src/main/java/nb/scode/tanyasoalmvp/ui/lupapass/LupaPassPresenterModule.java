package nb.scode.tanyasoalmvp.ui.lupapass;

import dagger.Module;
import dagger.Provides;

/**
 * Created by neobyte on 2/28/2017.
 */

@Module
public class LupaPassPresenterModule {
    LupaPassContract.View view;

    LupaPassPresenterModule(LupaPassContract.View view){
        this.view = view;
    }

    @Provides
    LupaPassContract.View providesLupaPassView(){
        return view;
    }
}
