package nb.scode.tanyasoalmvp.ui.chatroomtutor;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;

/**
 * Created by neobyte on 3/2/2017.
 */

public class ChatroomTutorPresenter implements ChatroomTutorContract.Presenter {

    private ChatroomTutorContract.View mView;
    private CloudDataTask dataTask;

    @Inject
    public ChatroomTutorPresenter(ChatroomTutorContract.View mView, CloudDataRepo dataTask) {
        this.mView = mView;
        this.dataTask = dataTask;
    }

    @Override
    public void start() {

    }
}
