package nb.scode.tanyasoalmvp.ui.chattanyatutor;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;

/**
 * Created by neobyte on 3/1/2017.
 */

public class ChatTanyaTutorPresenter implements ChatTanyaTutorContract.Presenter {

    private CloudDataTask dataTask;
    private ChatTanyaTutorContract.View view;

    @Inject
    public ChatTanyaTutorPresenter(CloudDataRepo dataTask, ChatTanyaTutorContract.View view) {
        this.dataTask = dataTask;
        this.view = view;
    }

    @Override
    public void start() {

    }
}
