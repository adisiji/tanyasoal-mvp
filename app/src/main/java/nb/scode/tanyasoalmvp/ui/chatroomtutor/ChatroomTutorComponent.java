package nb.scode.tanyasoalmvp.ui.chatroomtutor;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/2/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = ChatroomTutorPresenterModule.class)
public interface ChatroomTutorComponent {
    void inject (ChatroomTutorActivity chatroomTutorActivity);
}
