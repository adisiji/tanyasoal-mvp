package nb.scode.tanyasoalmvp.ui.chatroomtutor;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/2/2017.
 */

@Module
public class ChatroomTutorPresenterModule {

    private ChatroomTutorContract.View view;

    public ChatroomTutorPresenterModule(ChatroomTutorContract.View view) {
        this.view = view;
    }

    @Provides
    @ActScoped
    ChatroomTutorContract.View providesChatroomView (){
        return view;
    }
}
