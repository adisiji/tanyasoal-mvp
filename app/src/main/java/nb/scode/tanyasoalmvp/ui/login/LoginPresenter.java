package nb.scode.tanyasoalmvp.ui.login;

import android.util.Log;
import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataRepo;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataTask;

/**
 * Created by neobyte on 2/19/2017.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View view;
    private final CloudDataTask dataTask;
    private final PrefDataTask prefDataRepo;

    @Inject
    LoginPresenter(CloudDataRepo dataComponent, LoginContract.View view, PrefDataRepo dataRepo) {
        this.dataTask = dataComponent;
        this.view = view;
        this.prefDataRepo = dataRepo;
    }

    @Override
    public void start() {
        if(prefDataRepo.getAccessToken()!=null){
            //go to home
            view.gotoHome();
        }
        view.hideLoading();
    }

    @Override
    public void login(final String username, final String password) {
        view.delError();
        if(username.length()==0){
            view.errorEmptyUsername();
        }
        /*
         else if (!helper.isValidEmail(username)) {
         view.errorMail();
        }
         */
        else if(password.length()==0){
            view.errorEmptyPass();
        }
        else {
            dataTask.login(username, password, prefDataRepo.getFcmToken(), new CloudDataTask.Callback() {
                @Override
                public void process() {
                    view.showLoading();
                }

                @Override
                public void success(Object msg) {
                  prefDataRepo.saveAcccessToken((String)msg);
                  prefDataRepo.saveUsername(username);
                  dataTask.getProfile((String) msg, new CloudDataTask.Callback() {
                    @Override public void process() {

                    }

                    @Override public void success(Object object) {
                      view.hideLoading();
                      view.gotoHome();
                    }

                    @Override public void failed(String msg) {
                      view.hideLoading();
                      view.showError(msg);
                    }
                  });
                }

                @Override
                public void failed(String msg) {
                  Log.e(this.getClass().getName(), "failed: login");
                  view.hideLoading();
                  view.showError(msg);
                }
            });
        }
    }
}
