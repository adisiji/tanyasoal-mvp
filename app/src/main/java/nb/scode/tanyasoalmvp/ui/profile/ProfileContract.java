package nb.scode.tanyasoalmvp.ui.profile;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;
import nb.scode.tanyasoalmvp.models.Academics;

/**
 * Created by neobyte on 3/2/2017.
 */

public interface ProfileContract {

    interface Presenter extends BasePresenter {

      void simpan(String email,
          String username,
          String pass,
          String confpass,
          int jnsAkad,
          int tktAkad,
          String sekolah,
          String namaLkp,
          String noHp,
          String tgl);

      void setupAcademix();

      Academics getAcademix();

      void getProfile();

      void setLayout();

    }

    interface View extends BaseView<Presenter> {
      void errorBack();

      void setTitleName(String content);

      void setSpinner(Academics academics);

      void setNamaLengkap(String content);

      void setUsername(String content);

      void setTglLahir(String content);

      void setSchool(String content);

      void setNoHp(String content);

      void setAcademicLevel(int pos);

      void setAcademicType(int pos);

      void delError();

      void errorEmptyMail();

      void errorMail();

      void errorEmptyUsername();

      void errorMin5Username();

      void errorUsername();

      void errorEmptyPass();

      void errorMin6Pass();

      void errorPass();

      void errorEmptyConfPass();

      void errorConfPass();

      void errorEmptySekolah();

      void errorMin5Sekolah();

      void errorMin2NamaLkp();

      void errorEmptyNamaLkp();

      void errorEmptyHp();

      void erorHp();
    }

}
