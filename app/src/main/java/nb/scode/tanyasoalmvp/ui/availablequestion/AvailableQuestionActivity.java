package nb.scode.tanyasoalmvp.ui.availablequestion;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindArray;
import butterknife.BindView;
import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.models.Question;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;
import nb.scode.tanyasoalmvp.ui.chatroomtutor.ChatroomTutorActivity;
import nb.scode.tanyasoalmvp.ui.login.LoginActivity;

public class AvailableQuestionActivity extends BaseActivity implements AvailQuestContract.View{

    @Inject
    AvailQuestPresenter presenter;

    @BindArray(R.array.tkt_akademik_array)
    String[] tingkat;
    @BindArray(R.array.subjekItems)
    String[] subjeks;
    @BindView(R.id.rv_avail_question)
    RecyclerView rv_q;
    private List<Question> questions = new ArrayList<>();
    private Boolean isDoubleBackToExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_question);
        DaggerAvailQuestComponent.builder()
                .availQuestPresenterModule(new AvailQuestPresenterModule(this))
                .dataComponent(App.getDataComponent())
                .build().inject(this);
        isHome = true;
        getBackToolbar().setVisibility(View.GONE);
        setModelSoal();
        AvailQuestAdapter adapter = new AvailQuestAdapter(questions);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_q.setLayoutManager(mLayoutManager);
        rv_q.setItemAnimator(new DefaultItemAnimator());
        adapter.setAction(new AvailQuestAdapter.availAction() {
            @Override
            public void onClickCard(int pos, View view) {
                Intent i = new Intent(getApplicationContext(), ChatroomTutorActivity.class);
                startActivity(i);
            }
        });
        rv_q.setAdapter(adapter);
    }



    private void setModelSoal(){
        for(int i = 0;i<50;i++){
            Question q = new Question();
            q.setTutor("Joni Handoko");
            q.setLvl(tingkat[i%5]);
            q.setNama("Ney Darmawan");
            q.setSubjek(subjeks[i%5]);
            q.setSoal("Nomor 2 dan 3 ya kak");
            q.setPeriode(12);
            if(i%2==0){
                q.setJawaban("Foto");
            }
            else {
                q.setJawaban("Video");
            }
            q.setSisaw(SystemClock.currentThreadTimeMillis());
            questions.add(q);
        }
    }

    @Override
    public void onBackPressed() {

        if (drawerFragment.isDrawerOpen()) {

            drawerFragment.closeDrawer();

        } else {

            if (isDoubleBackToExit) {
                super.onBackPressed();
                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);
                finish();
            }
            if (!isDoubleBackToExit) {
                Toast.makeText(this, getString(R.string.tap_exit), Toast.LENGTH_SHORT).show();
            }
            this.isDoubleBackToExit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isDoubleBackToExit = false;
                }
            }, 2000); //delay 2 detik
        }
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
    }
}
