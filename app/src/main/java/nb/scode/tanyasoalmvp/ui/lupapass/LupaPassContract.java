package nb.scode.tanyasoalmvp.ui.lupapass;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 2/28/2017.
 */

public interface LupaPassContract {

    interface Presenter extends BasePresenter {

        void sendForm(String email,
                     String namalkp,
                     String noHp,
                     String tgl);

    }

    interface View extends BaseView<Presenter> {

        void delError();

        void errorEmptyMail();

        void errorMin2NamaLkp();

        void errorEmptyNamaLkp();

        void errorEmptyHp();

        void erorHp();

        void successLupaPass();

    }

}
