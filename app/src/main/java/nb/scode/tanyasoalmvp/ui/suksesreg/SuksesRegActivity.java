package nb.scode.tanyasoalmvp.ui.suksesreg;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import butterknife.OnClick;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;
import nb.scode.tanyasoalmvp.ui.homesiswa.HomeSiswaActivity;
import nb.scode.tanyasoalmvp.ui.tanyatutor.TanyaTutorActivity;

/**
 * Created by neobyte on 3/27/2017.
 */

public class SuksesRegActivity extends BaseActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sukses_reg);
    getBackToolbar().setVisibility(View.GONE);
    getMail().setVisibility(View.GONE);
  }

  @OnClick(R.id.btn_mulai_bertanya)
  void mulaitanya(){
    Intent i = new Intent(getApplicationContext(), TanyaTutorActivity.class);
    i.putExtra(FROM_SUCCESS,true);
    startActivity(i);
    finish();
  }

  @OnClick(R.id.btn_kembali_homepage)
  void back2home(){
    Intent i = new Intent(getApplicationContext(), HomeSiswaActivity.class);
    startActivity(i);
    finish();
  }

}
