package nb.scode.tanyasoalmvp.ui.isivoucher;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 3/9/2017.
 */

public interface IsiVoucherContract {

    interface Presenter extends BasePresenter {
        void cekVoucher(String voucher);
    }

    interface View extends BaseView<Presenter> {
        void errorEmptyVoucher();

        void errorMinVoucher();
    }

}
