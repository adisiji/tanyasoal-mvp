package nb.scode.tanyasoalmvp.ui.layanantanyatutor;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/1/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = LayananTanyaTutorPresenterModule.class)
public interface LayananTanyaTutorComponent {
    void inject(LayananTanyaTutorActivity layananTanyaTutorActivity);
}
