package nb.scode.tanyasoalmvp.ui.isivoucher;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/9/2017.
 */

@Module
public class IsiVoucherModule {

    private IsiVoucherContract.View mView;

    public IsiVoucherModule(IsiVoucherContract.View view) {

        mView = view;
    }

    @Provides
    @ActScoped
    IsiVoucherContract.View providesIsiVoucherView(){
        return mView;
    }

}
