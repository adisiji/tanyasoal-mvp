package nb.scode.tanyasoalmvp.ui.tanyatutor;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/9/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = TanyaTutorModule.class)
public interface TanyaTutorComponent {
    void inject (TanyaTutorActivity tanyaTutorActivity);
}
