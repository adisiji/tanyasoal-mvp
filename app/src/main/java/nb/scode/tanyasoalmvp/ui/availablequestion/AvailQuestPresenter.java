package nb.scode.tanyasoalmvp.ui.availablequestion;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;

/**
 * Created by neobyte on 3/2/2017.
 */

public class AvailQuestPresenter implements AvailQuestContract.Presenter {

    private AvailQuestContract.View mView;
    private CloudDataTask dataTask;

    @Inject
    public AvailQuestPresenter(AvailQuestContract.View mView, CloudDataRepo dataTask) {
        this.mView = mView;
        this.dataTask = dataTask;
    }

    @Override
    public void start() {

    }
}
