package nb.scode.tanyasoalmvp.ui.lupapass;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 2/28/2017.
 */

@ActScoped
@Component(dependencies = DataComponent.class, modules = LupaPassPresenterModule.class)
interface LupaPassComponent {

    void inject(LupaPassActivity activity);

}
