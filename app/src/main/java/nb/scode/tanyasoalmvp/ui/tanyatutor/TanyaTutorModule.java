package nb.scode.tanyasoalmvp.ui.tanyatutor;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/4/2017.
 */

@Module
public class TanyaTutorModule {

    private TanyaTutorContract.View mView;

    public TanyaTutorModule(TanyaTutorContract.View view) {

        mView = view;
    }

    @Provides
    @ActScoped
    TanyaTutorContract.View providesTanyaTutorView(){
        return mView;
    }
}
