package nb.scode.tanyasoalmvp.ui.isivoucher;

import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import custom_font.EditTextMyriad;
import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;

public class IsiVoucherActivity extends BaseActivity implements IsiVoucherContract.View {

    @Inject IsiVoucherPresenter mPresenter;

    @BindView(R.id.ed_kode_voucher)
    EditTextMyriad editTextVoucher;
    @BindString(R.string.error_field_empty)
    String fieldEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_voucher);
        DaggerIsiVoucherComponent.builder().dataComponent(App.getDataComponent())
                .isiVoucherModule(new IsiVoucherModule(this))
                .build().inject(this);
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_kirim_voucher)
    void kirimVoucher() {
        editTextVoucher.setError(null);
        String kdVoucher = editTextVoucher.getText().toString().trim();
    }

    @Override
    public void errorEmptyVoucher() {
        editTextVoucher.setError(fieldEmpty);
    }

    @Override
    public void errorMinVoucher() {
        editTextVoucher.setError("Minimum 24 characters");
    }
}
