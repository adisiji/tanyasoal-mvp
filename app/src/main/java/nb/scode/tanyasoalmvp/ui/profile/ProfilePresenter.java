package nb.scode.tanyasoalmvp.ui.profile;

import javax.inject.Inject;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataRepo;
import nb.scode.tanyasoalmvp.data.prefdata.PrefDataTask;
import nb.scode.tanyasoalmvp.modelretro.ProfileDetail;
import nb.scode.tanyasoalmvp.models.Academics;
import nb.scode.tanyasoalmvp.util.Helper;

/**
 * Created by neobyte on 3/2/2017.
 */

public class ProfilePresenter implements ProfileContract.Presenter {

  private CloudDataTask mDataTask;
  private ProfileContract.View mView;
  private PrefDataTask prefDataTask;
  private Helper helper;

  @Inject public ProfilePresenter(PrefDataRepo prefDataTask, CloudDataRepo dataTask,
      ProfileContract.View view) {
    this.prefDataTask = prefDataTask;
    mDataTask = dataTask;
    mView = view;
    helper = new Helper();
  }

  @Override public void start() {
    setupAcademix();
  }

  @Override public void simpan(String email, String username, String pass, String confpass,
      int jnsAkad, int tktAkad, String sekolah, String namaLkp, String noHp, String tgl) {
    mDataTask.changeProfile(email, namaLkp, noHp, tgl, jnsAkad, tktAkad, sekolah,
        new CloudDataTask.Callback() {
          @Override public void process() {
            mView.showLoading();
          }

          @Override public void success(Object object) {
            mView.hideLoading();
          }

          @Override public void failed(String msg) {
            mView.hideLoading();
            mView.showError(msg);
          }
        });
  }

  @Override public void setupAcademix() {
    mDataTask.getPredefines(new CloudDataTask.Callback() {
      @Override public void process() {
        mView.showLoading();
      }

      @Override public void success(Object object) {
        mView.setSpinner(getAcademix());
        getProfile();
      }

      @Override public void failed(String msg) {
        mView.hideLoading();
        mView.showError(msg);
        mView.errorBack();
      }
    });
  }

  @Override public Academics getAcademix() {
    return mDataTask.getAcademix();
  }

  @Override public void getProfile() {
    if (mDataTask.getProfileDetail() == null) {
      mDataTask.getProfile(prefDataTask.getAccessToken(), new CloudDataTask.Callback() {
        @Override public void process() {

        }

        @Override public void success(Object object) {
          setLayout();
          mView.hideLoading();
        }

        @Override public void failed(String msg) {
          mView.hideLoading();
        }
      });
    } else {
      setLayout();
      mView.hideLoading();
    }
  }

  @Override public void setLayout() {
    ProfileDetail profileDetail = mDataTask.getProfileDetail();
    //prepare layout
    mView.setTitleName(profileDetail.getName());
    if (profileDetail.getAcademicLevel() == null) {
      mView.setAcademicLevel(0);
    } else {
      mView.setAcademicLevel(profileDetail.getAcademicLevel());
    }
    mView.setAcademicType(profileDetail.getAcademicType());
    mView.setUsername(prefDataTask.getUsername());
    mView.setNamaLengkap(profileDetail.getName());
    mView.setNoHp(profileDetail.getPhonenumber());
    mView.setSchool(profileDetail.getSchool());
    mView.setTglLahir(helper.dateConvert(profileDetail.getDateofbirth()));
  }
}
