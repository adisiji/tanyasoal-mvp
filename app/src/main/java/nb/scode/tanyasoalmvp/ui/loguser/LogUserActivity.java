package nb.scode.tanyasoalmvp.ui.loguser;

import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;

import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.models.LogUserAct;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;

import static custom_font.TextViewMyriad.ANDROID_SCHEMA;

public class LogUserActivity extends BaseActivity implements LogUserConract.View {

    @Inject
    LogUserPresenter presenter;

    private final int sdk = android.os.Build.VERSION.SDK_INT;

    @BindView(R.id.table_cat_aktivitas)
    TableLayout tabelAktivitas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_user);
        DaggerLogUserComponent.builder()
                .dataComponent(App.getDataComponent())
                .logUserPresenterModule(new LogUserPresenterModule(this))
                .build().inject(this);
        isBack = true;
        setModelAktivitas();
    }

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    private void setModelAktivitas(){
        for(int i = 0;i<50;i++){
            LogUserAct q = new LogUserAct();
            long now = System.currentTimeMillis()/1000;
            String currentDateTime = dateFormat.format(now); // Find todays date
            q.setWaktu(now);
            if(i%2==0) {
                q.setAksi("Tanya Tutor");
                q.setSaldo_ptg("Rp"+String.valueOf(4000+i*2000)+",-"); }
            else {
                q.setAksi("Isi Saldo");
                q.setSaldo_ptg("");
            }

            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            if(i%2==0){
                tableRow.setBackgroundColor(getResources().getColor(R.color.soft_grey));
            }
            ArrayList<TextView> ready = new ArrayList<>();
            TextView waktu = new TextView(this);
            waktu.setText(currentDateTime);
            ready.add(waktu);
            TextView aksi = new TextView(this);
            aksi.setText(q.getAksi());
            ready.add(aksi);
            TextView saldo = new TextView(this);
            saldo.setText(String.valueOf(q.getSaldo_ptg()));
            ready.add(saldo);
            int j = ready.size();
            for(int k = 0; k<j; k++) {
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    ready.get(k).setBackgroundDrawable( getResources().getDrawable(R.drawable.cellborder) );
                } else {
                    ready.get(k).setBackground( getResources().getDrawable(R.drawable.cellborder));
                }
                ready.get(k).setLayoutParams(new TableRow.LayoutParams(50, ViewGroup.LayoutParams.MATCH_PARENT));
                ready.get(k).setPadding(3,3,3,3);
                ready.get(k).setGravity(Gravity.CENTER_HORIZONTAL);
                tableRow.addView(ready.get(k));
            }
            tabelAktivitas.addView(tableRow);
        }
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
    }
}
