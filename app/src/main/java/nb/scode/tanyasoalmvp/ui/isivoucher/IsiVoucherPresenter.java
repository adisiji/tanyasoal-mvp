package nb.scode.tanyasoalmvp.ui.isivoucher;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;

/**
 * Created by neobyte on 3/9/2017.
 */

public class IsiVoucherPresenter implements IsiVoucherContract.Presenter {

    private final CloudDataTask mCloudDataTask;
    private final IsiVoucherContract.View mView;

    @Inject
    public IsiVoucherPresenter(CloudDataRepo cloudDataTask, IsiVoucherContract.View view) {

        mCloudDataTask = cloudDataTask;
        mView = view;
    }

    @Override
    public void start() {

    }

    @Override
    public void cekVoucher(String voucher) {
        if(voucher.length() == 0){
            mView.errorEmptyVoucher();
        }
        else if(voucher.length()<24){
            mView.errorMinVoucher();
        } else {
          //TODO : send voucher to server
        }
    }
}
