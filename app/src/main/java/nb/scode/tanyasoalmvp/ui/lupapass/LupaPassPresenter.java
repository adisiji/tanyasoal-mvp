package nb.scode.tanyasoalmvp.ui.lupapass;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;
import nb.scode.tanyasoalmvp.util.Helper;

/**
 * Created by neobyte on 2/28/2017.
 */

public class LupaPassPresenter implements LupaPassContract.Presenter {

    private LupaPassContract.View view;
    private CloudDataTask dataTask;
    private Helper helper;

    @Inject
    LupaPassPresenter(LupaPassContract.View view, CloudDataRepo dataTask) {
        this.view = view;
        this.dataTask = dataTask;
        helper = new Helper();
    }

    @Override
    public void start() {

    }

    @Override
    public void sendForm(String username,
                        String namaLkp,
                        String noHp,
                        String tgl) {
        view.delError();
        int e = 0;
        if (username.length() == 0) {
            view.errorEmptyMail();
            e++;
        }
        if (namaLkp.length() == 0) {
            view.errorEmptyNamaLkp();
            e++;
        } else if (namaLkp.length() < 2) {
            view.errorMin2NamaLkp();
            e++;
        }
        if (noHp.length() == 0) {
            view.errorEmptyHp();
            e++;
        } else if (!helper.isValidPhone(noHp)) {
            view.erorHp();
            e++;
        }
        if (e == 0) {
            dataTask.lupaPass(username, namaLkp, noHp, tgl, new CloudDataTask.Callback() {
                @Override
                public void process() {
                    view.showLoading();
                }

                @Override
                public void success(Object msg) {
                    view.hideLoading();
                    view.successLupaPass();
                }

                @Override
                public void failed(String msg) {
                    view.hideLoading();
                    view.showError(msg);
                }
            });
        }
    }
}