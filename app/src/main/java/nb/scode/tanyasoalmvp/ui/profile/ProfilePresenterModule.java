package nb.scode.tanyasoalmvp.ui.profile;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/2/2017.
 */

@Module
public class ProfilePresenterModule {

    private ProfileContract.View mView;

    public ProfilePresenterModule(ProfileContract.View view) {

        mView = view;
    }

    @ActScoped
    @Provides
    ProfileContract.View providesProfileView(){
        return mView;
    }
}
