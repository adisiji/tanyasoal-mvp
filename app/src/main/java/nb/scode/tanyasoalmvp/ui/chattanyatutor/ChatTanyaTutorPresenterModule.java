package nb.scode.tanyasoalmvp.ui.chattanyatutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by neobyte on 3/1/2017.
 */

@Module
public class ChatTanyaTutorPresenterModule {

    private ChatTanyaTutorContract.View mView;

    public ChatTanyaTutorPresenterModule(ChatTanyaTutorContract.View mView) {
        this.mView = mView;
    }

    @Provides
    ChatTanyaTutorContract.View providesChatTanyaTutorView() {
        return mView;
    }
}
