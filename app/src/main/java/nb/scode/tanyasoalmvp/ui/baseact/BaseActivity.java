package nb.scode.tanyasoalmvp.ui.baseact;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import custom_font.TextViewApple;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.homesiswa.HomeSiswaActivity;
import nb.scode.tanyasoalmvp.ui.login.LoginActivity;
import nb.scode.tanyasoalmvp.ui.profile.ProfileActivity;

/**
 * Created by User on 12/24/2016.
 */

public class BaseActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_mail_toolbar) ImageView mail;
    @BindView(R.id.toolbar_back_btn)
    TextViewApple btnBack;
    protected FragmentDrawer drawerFragment;
    protected ProgressDialog mDialog = null;
    protected boolean isHome = false;
    protected boolean isBack = false;
    protected final String FROM_SUCCESS = "FROM_SUCCESS";

    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    private static final String USER_NAME = "USER_NAME";
    private SharedPreferences sharedPreferences;

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    @Override
    public void setContentView(int layoutResID)
    {
        final String PREF_NAME = "worldhere";
        sharedPreferences = getApplication().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        DrawerLayout fullView = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);
        ButterKnife.bind(this,fullView);
        drawerFragment = (FragmentDrawer) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        setSupportActionBar(toolbar);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        toolbar.setTitle("");
        if(mDialog == null) {
            mDialog = new ProgressDialog(this);
            mDialog.setMessage("Please wait...");
            mDialog.setCancelable(false);
        }
    }

    @OnClick(R.id.toolbar_back_btn)
    void back(){
        onBackPressed();
    }

    @OnClick(R.id.pict_profile_toolbar)
    public void showProfile(){
        if(getToolbar()){
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(i);
        }
    }

    private void displayView(int pos){
        switch (pos) {
            case 0: { //home
                //TODO : implement if user / tutor
                if (!isHome && !isBack) {
                    Intent i = new Intent(getApplicationContext(), HomeSiswaActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }
                else if(isBack)
                    onBackPressed();
                break;
            }
            case 1: { //layanan kami
                break;
            }
            case 2:{ //syllabus
                break;
            }
            case 3: { //FAQ
                break;
            }
            case 4: { //Tentang kami
                break;
            }
            case 5: { //Logout
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                sharedPreferences.edit().putString(ACCESS_TOKEN, null).apply();
                break;
            }
        }
    }

    protected TextView getBackToolbar() {
        return btnBack;
    }

    protected boolean getToolbar(){
        return true;
    }

    protected ImageView getMail(){
        return mail;
    }

}