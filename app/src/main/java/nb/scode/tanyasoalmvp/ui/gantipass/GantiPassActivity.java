package nb.scode.tanyasoalmvp.ui.gantipass;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import custom_font.EditTextMyriad;
import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseFirstActivity;
import nb.scode.tanyasoalmvp.ui.login.LoginActivity;

public class GantiPassActivity extends BaseFirstActivity implements GantiPassContract.View {

    @Inject
    GantiPassPresenter presenter;
    private String username = null, pass, confpass;;
    @BindString(R.string.error_field_empty)
    String emptyfield;
    @BindView(R.id.txt_create_password)
    EditTextMyriad et_pass;
    @BindView(R.id.txt_create_confpass)
    EditTextMyriad et_conf_pass;
    @BindString(R.string.error_password_match)
    String errorPass;
    @BindString(R.string.error_wrong_format)
    String wrongFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_pass);
        DaggerGantiPassComponent.builder().dataComponent(App.getDataComponent())
                .gantiPassPresenterModule(new GantiPassPresenterModule(this))
                .build().inject(this);

        try{
            Intent i = getIntent();
            username = i.getStringExtra("username");
        }
        catch (NullPointerException e){
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);
        }

    }

    @OnClick(R.id.btn_simpan)
    void resetPass(){
        pass = et_pass.getText().toString().trim();
        confpass = et_conf_pass.getText().toString().trim();

        if(presenter.cekForm(pass,confpass)){
            presenter.sendForm(username,pass,confpass);
        }
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void errorConfPass() {
        et_conf_pass.setError(errorPass);
    }

    @Override
    public void errorEmptyConfPass() {
        et_conf_pass.setError(emptyfield);
    }

    @Override
    public void errorPass() {
        et_pass.setError(wrongFormat);
    }

    @Override
    public void errorMin6Pass() {
        et_pass.setError(getString(R.string.error_min_6_char));
    }

    @Override
    public void errorEmptyPass() {
        et_pass.setError(emptyfield);
    }
}
