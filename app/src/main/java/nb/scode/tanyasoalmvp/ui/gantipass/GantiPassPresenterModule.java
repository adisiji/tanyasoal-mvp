package nb.scode.tanyasoalmvp.ui.gantipass;

import dagger.Module;
import dagger.Provides;

/**
 * Created by neobyte on 2/28/2017.
 */

@Module
public class GantiPassPresenterModule {

    private GantiPassContract.View view;

    public GantiPassPresenterModule (GantiPassContract.View view){
        this.view = view;
    }

    @Provides
    GantiPassContract.View providesGantipassView(){
        return view;
    }
}
