package nb.scode.tanyasoalmvp.ui.signup;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;
import nb.scode.tanyasoalmvp.models.Academics;

/**
 * Created by neobyte on 2/28/2017.
 */

public interface SignupContract {

    interface Presenter extends BasePresenter {

        void signup(String email,
                    String username,
                    String pass,
                    String confpass,
                    int jnsAkad,
                    int tktAkad,
                    String sekolah,
                    String promo,
                    String namaLkp,
                    String noHp,
                    String tgl);

        void setupAcademix();

        Academics getAcademix();

    }

    interface View extends BaseView<Presenter> {

        void backToLogin();

        void setSpinner(Academics academics);

        void delError();

        void errorEmptyMail();

        void errorMail();

        void errorEmptyUsername();

        void errorMin5Username();

        void errorUsername();

        void errorEmptyPass();

        void errorMin6Pass();

        void errorPass();

        void errorEmptyConfPass();

        void errorConfPass();

        void errorEmptySekolah();

        void errorMin5Sekolah();

        void errorPromo();

        void errorMin2NamaLkp();

        void errorEmptyNamaLkp();

        void errorEmptyHp();

        void erorHp();

        void successSignUp();

    }

}
