package nb.scode.tanyasoalmvp.ui.login;

import nb.scode.tanyasoalmvp.BasePresenter;
import nb.scode.tanyasoalmvp.BaseView;

/**
 * Created by neobyte on 2/19/2017.
 */

public interface LoginContract {

    interface Presenter extends BasePresenter {

        void login(String username, String password);

    }

    interface View extends BaseView<Presenter> {

        void delError();

        void errorEmptyUsername();

        //void errorMail();

        void errorEmptyPass();

        void errorPass();

        void gotoHome();

    }
}
