package nb.scode.tanyasoalmvp.ui.tanyatutor;

import javax.inject.Inject;

import nb.scode.tanyasoalmvp.data.clouddata.CloudDataRepo;
import nb.scode.tanyasoalmvp.data.clouddata.CloudDataTask;

/**
 * Created by neobyte on 3/9/2017.
 */

public class TanyaTutorPresenter implements TanyaTutorContract.Presenter {

    private final TanyaTutorContract.View view;
    private final CloudDataTask dataTask;

    @Override
    public void start() {

    }

    @Inject
    public TanyaTutorPresenter(TanyaTutorContract.View view, CloudDataRepo dataTask) {

        this.view = view;
        this.dataTask = dataTask;
    }
}
