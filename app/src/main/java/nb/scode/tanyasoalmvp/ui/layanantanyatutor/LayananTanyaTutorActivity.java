package nb.scode.tanyasoalmvp.ui.layanantanyatutor;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.models.Question;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;
import nb.scode.tanyasoalmvp.ui.chattanyatutor.ChatroomTanyaTutorActivity;
import nb.scode.tanyasoalmvp.ui.homesiswa.HomeSiswaActivity;

public class LayananTanyaTutorActivity extends BaseActivity implements LayananTanyaTutorContract.View{

    @Inject
    LayananTanyaTutorPresenter presenter;

    @BindView(R.id.gridview)
    GridView gridView;
    @BindView(R.id.rg_subjek_lay_tanya)
    RadioGroup rgSubjek;
    @BindView(R.id.activity_layanan_tanya_tutor)
    RelativeLayout relativeLayout;

    private boolean defBack;
    private LayTnyTutorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_layanan_tanya_tutor);
      DaggerLayananTanyaTutorComponent.builder()
          .dataComponent(App.getDataComponent())
          .layananTanyaTutorPresenterModule(new LayananTanyaTutorPresenterModule(this))
          .build().inject(this);
      defBack = getIntent().getBooleanExtra(FROM_SUCCESS,false);
      isBack = true;
      Question cards[] = new Question[50];
      for(int i = 0;i<cards.length;i++){
        Question card = new Question();
        card.setSoal("Apa nama benda ini ?");
        cards[i] = card;
      }
      adapter = new LayTnyTutorAdapter(this,cards);
      adapter.SetActionCard(new LayTnyTutorAdapter.ActionCard() {
        @Override
        public void onClick(int position) {
          Log.e("User click = ",String.valueOf(position));
          Intent i = new Intent(getApplicationContext(),ChatroomTanyaTutorActivity.class);
          startActivity(i);
        }
      });
    }

    @Override
    protected void onResume() {
        super.onResume();
        gridView.setAdapter(adapter);
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(defBack){
            Intent i = new Intent(getApplicationContext(), HomeSiswaActivity.class);
            startActivity(i);
            finish();
        }
    }
}
