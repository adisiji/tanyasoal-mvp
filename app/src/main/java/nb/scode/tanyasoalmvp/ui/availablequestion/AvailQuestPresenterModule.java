package nb.scode.tanyasoalmvp.ui.availablequestion;

import dagger.Module;
import dagger.Provides;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 3/2/2017.
 */

@Module
public class AvailQuestPresenterModule {

    private AvailQuestContract.View view;

    public AvailQuestPresenterModule(AvailQuestContract.View view) {
        this.view = view;
    }

    @Provides
    @ActScoped
    AvailQuestContract.View providesAvailQuestView(){
        return view;
    }
}
