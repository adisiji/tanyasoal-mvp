package nb.scode.tanyasoalmvp.ui.profile;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import com.jaredrummler.materialspinner.MaterialSpinner;
import custom_font.EditTextMyriad;
import custom_font.TextViewApple;
import es.dmoral.toasty.Toasty;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.models.AcademicLevels;
import nb.scode.tanyasoalmvp.models.AcademicTypes;
import nb.scode.tanyasoalmvp.models.Academics;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;
import nb.scode.tanyasoalmvp.util.DateTextWatcher;

public class ProfileActivity extends BaseActivity implements ProfileContract.View {

  @BindView(R.id.title_profile) TextViewApple titleProfile;
  @BindView(R.id.txt_ed_name) EditTextMyriad etUsername;
  @BindView(R.id.txt_ed_email) EditTextMyriad etEmail;
  @BindView(R.id.txt_ganti_pass) EditTextMyriad et_gantipass;
  @BindView(R.id.txt_ed_confpass) EditTextMyriad et_konfpass;
  @BindView(R.id.txt_ed_sekolah) EditTextMyriad et_sekolah;
  @BindView(R.id.txt_nama_lkp) EditTextMyriad et_fullname;
  @BindView(R.id.txt_no_hp) EditTextMyriad et_hp;
  @BindView(R.id.ed_tgl_lhr) EditTextMyriad et_tlh;
  @BindView(R.id.txt_create_jenakad) MaterialSpinner spinJenAkad;
  @BindView(R.id.spin_tingkat_akad) MaterialSpinner spinTktAkad;
  @BindViews({ R.id.root_jnsAkad, R.id.root_sekolah, R.id.root_tktAkad }) List<LinearLayout>
      layoutList;
  @BindViews({ R.id.cat_aktivitas_profile, R.id.header_profile }) List<TextView> textViewHeader;

  @BindString(R.string.error_field_empty) String emptyfield;
  @BindString(R.string.failed_profile_edit) String failedEdit;
  @BindString(R.string.success_profile_edit) String successEdit;

  @Inject ProfilePresenter mPresenter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile);
    DateTextWatcher dateTextWatcher = new DateTextWatcher(et_tlh);
    et_tlh.addTextChangedListener(dateTextWatcher);
    DaggerProfileComponent.builder()
        .dataComponent(App.getDataComponent())
        .profilePresenterModule(new ProfilePresenterModule(this))
        .build()
        .inject(this);

    mPresenter.start();
  }

  @OnClick(R.id.btn_simpan) void simpanProfile() {
    mPresenter.simpan(etEmail.getText().toString(), etUsername.getEditableText().toString(),
        et_gantipass.getEditableText().toString(), et_konfpass.getEditableText().toString(),
        spinJenAkad.getSelectedIndex(), spinTktAkad.getSelectedIndex(),
        et_sekolah.getEditableText().toString(), et_fullname.getEditableText().toString(),
        et_hp.getEditableText().toString(), et_tlh.getEditableText().toString());
  }

  @OnClick(R.id.btn_kembali) void backiskembali() {
    onBackPressed();
  }

  @Override public void hideLoading() {
    mDialog.dismiss();
  }

  @Override public void showLoading() {
    mDialog.show();
  }

  @Override public void showError(String msg) {
    Toasty.error(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
  }

  @Override public void errorBack() {
    onBackPressed();
  }

  @Override public void setTitleName(String content) {
    titleProfile.setText(getString(R.string.selamat_datang) + " " + content);
  }

  @Override public void setSpinner(Academics academics) {
    List<String> typeName = new ArrayList<>();
    for (AcademicTypes academicTypes : academics.getAcademicTypeList()) {
      typeName.add(academicTypes.getName());
    }
    List<String> levelName = new ArrayList<>();
    for (AcademicLevels academicLevels : academics.getAcademicLevelList()) {
      levelName.add(academicLevels.getName());
    }
    spinJenAkad.setItems(typeName);
    spinTktAkad.setItems(levelName);
  }

  @Override public void setNamaLengkap(String content) {
    etUsername.setText(content);
    et_fullname.setText(content);
  }

  @Override public void setUsername(String content) {
    etEmail.setText(content);
  }

  @Override public void setSchool(String content) {
    et_sekolah.setText(content);
  }

  @Override public void setTglLahir(String content) {
    et_tlh.setText(content);
  }

  @Override public void setNoHp(String content) {
    et_hp.setText(content);
  }

  @Override public void setAcademicLevel(int pos) {
    spinTktAkad.setSelectedIndex(pos);
  }

  @Override public void setAcademicType(int pos) {
    spinJenAkad.setSelectedIndex(pos);
  }

  @Override public void erorHp() {
    et_hp.setError(getString(R.string.error_phone));
  }

  @Override public void errorEmptyHp() {
    et_hp.setError(emptyfield);
  }

  @Override public void errorMin2NamaLkp() {
    etUsername.setError(getString(R.string.error_min_2_char));
  }

  @Override public void errorEmptyNamaLkp() {
    etUsername.setError(emptyfield);
  }

  @Override public void errorMin5Sekolah() {
    et_sekolah.setError(getString(R.string.error_min_5_char));
  }

  @Override public void errorEmptySekolah() {
    et_sekolah.setError(emptyfield);
  }

  @Override public void errorConfPass() {
    et_konfpass.setError(getString(R.string.error_wrong_format));
  }

  @Override public void errorEmptyConfPass() {
    et_konfpass.setError(emptyfield);
  }

  @Override public void errorPass() {
    et_gantipass.setError(getString(R.string.error_wrong_format));
  }

  @Override public void errorMin6Pass() {
    et_gantipass.setError(getString(R.string.error_min_6_char));
  }

  @Override public void errorEmptyPass() {
    et_gantipass.setError(emptyfield);
  }

  @Override public void errorUsername() {
    etEmail.setError(getString(R.string.error_wrong_format));
  }

  @Override public void errorMin5Username() {
    etEmail.setError(getString(R.string.error_min_5_char));
  }

  @Override public void errorEmptyUsername() {
    etEmail.setError(emptyfield);
  }

  @Override public void errorMail() {
    //tv_email.setError(getString(R.string.error_wrong_format));
  }

  @Override public void errorEmptyMail() {
    //tv_email.setError(emptyfield);
  }

  @Override public void delError() {
    etUsername.setError(null);
    et_fullname.setError(null);
    et_gantipass.setError(null);
    et_hp.setError(null);
    et_sekolah.setError(null);
    etEmail.setError(null);
  }
}
