package nb.scode.tanyasoalmvp.ui.isisaldo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import nb.scode.tanyasoalmvp.App;
import nb.scode.tanyasoalmvp.R;
import nb.scode.tanyasoalmvp.ui.baseact.BaseActivity;
import nb.scode.tanyasoalmvp.ui.isivoucher.IsiVoucherActivity;

public class IsiSaldoActivity extends BaseActivity implements IsiSaldoContract.View {

    @Inject
    IsiSaldoPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_saldo);
        DaggerIsiSaldoComponent.builder()
                .dataComponent(App.getDataComponent())
                .isiSaldoPresenterModule(new IsiSaldoPresenterModule(this))
                .build().inject(this);
        isBack = true;
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void showLoading() {
        mDialog.show();
    }

    @Override
    public void showError(String msg) {
        Toasty.error(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_trf_atm)
    void viaTransfer(){
        /*
        Intent i = new Intent(getApplicationContext(), TrfAtmActivity.class);
        startActivity(i);
        */
    }

    @OnClick(R.id.btn_inetbank)
    void viaIBanking(){
        /*
        Intent i = new Intent(getApplicationContext(), InetBankingActivity.class);
        startActivity(i);
        */
    }

    @OnClick(R.id.btn_voucher)
    void viaVoucher(){
        Intent i = new Intent (getApplicationContext(), IsiVoucherActivity.class);
        startActivity(i);
    }
}
