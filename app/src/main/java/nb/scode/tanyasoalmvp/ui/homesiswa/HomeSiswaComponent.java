package nb.scode.tanyasoalmvp.ui.homesiswa;

import dagger.Component;
import nb.scode.tanyasoalmvp.data.DataComponent;
import nb.scode.tanyasoalmvp.di.scope.ActScoped;

/**
 * Created by neobyte on 2/19/2017.
 */

@ActScoped
@Component (dependencies = DataComponent.class, modules = HomeSiswaPresenterModule.class)
public interface HomeSiswaComponent {

    void inject(HomeSiswaActivity activity);

}
