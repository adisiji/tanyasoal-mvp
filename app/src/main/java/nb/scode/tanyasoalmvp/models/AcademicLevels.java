package nb.scode.tanyasoalmvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neobyte on 1/28/2017.
 */

public class AcademicLevels {
    @SerializedName("i")
    @Expose
    private int id;
    @SerializedName("n")
    @Expose
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
