package nb.scode.tanyasoalmvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by neobyte on 1/28/2017.
 */

public class Academics {

    @SerializedName("academicLevel")
    @Expose
    private List<AcademicLevels> academicLevels = new ArrayList<>();

    @SerializedName("academicTypes")
    @Expose
    private List<AcademicTypes> academicTypes = new ArrayList<>();

    public List<AcademicLevels> getAcademicLevelList() {
        return academicLevels;
    }

    public void setAcademicLevelList(List<AcademicLevels> academicLevels) {
        this.academicLevels = academicLevels;
    }

    public List<AcademicTypes> getAcademicTypeList() {
        return academicTypes;
    }

    public void setAcademicTypeList(List<AcademicTypes> academicTypes) {
        this.academicTypes = academicTypes;
    }
}
